package org.example.repo;

public abstract class Operation<T> {
     abstract public T compute() throws ArithmeticException;
}
