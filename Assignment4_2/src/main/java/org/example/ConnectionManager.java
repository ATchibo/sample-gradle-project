package org.example;

import org.example.exceptions.AddConnectionException;
import org.example.exceptions.AddPendingConnectionException;

import javax.json.Json;
import java.io.IOException;
import java.io.StringWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.example.Constants.DEFAULT_USERNAME;
import static org.example.Constants.DISCONNECT;
import static org.example.Constants.DISCONNECTED_FROM_EVERYONE;
import static org.example.Constants.ERROR_ADDING_CONNECTION;
import static org.example.Constants.ERROR_PENDING_CONNECTION;
import static org.example.Constants.MESSAGE_KEY;
import static org.example.Constants.USERNAME_KEY;
import static org.example.Constants.YOU_DISCONNECTED;

public class ConnectionManager {

    private static ConnectionManager instance = null;

    public static ConnectionManager getInstance() {
        if (instance == null)
            instance = new ConnectionManager();
        return instance;
    }

    private final Map<Socket, ReadThread> connections;
    private String username;

    private ConnectionManager() {
        connections = new HashMap<>();
        username = DEFAULT_USERNAME.value;
    }

    public void addPendingConnection(Socket socket) throws AddPendingConnectionException {
        try {
            ReadThread readThread = new ReadThread(socket);
            connections.put(socket, readThread);

        } catch (IOException e) {
            throw new AddPendingConnectionException(ERROR_PENDING_CONNECTION.value + e.getMessage());
        }
    }

    public void addConnection(Socket socket) throws AddConnectionException {
        try {
            ReadThread readThread = new ReadThread(socket);
            connections.put(socket, readThread);
            readThread.start();

        } catch (IOException e) {
            throw new AddConnectionException(ERROR_ADDING_CONNECTION.value + e.getMessage());
        }
    }

    public boolean awakeConnection(String address, int port) {
        for (Socket socket : connections.keySet()) {
            if (socket.getPort() == port) {
                connections.get(socket).start();
                return true;
            }
        }
        return false;
    }

    public void writeMessage(String message) {
        StringWriter stringWriter = new StringWriter();
        Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                .add(USERNAME_KEY.value, username)
                .add(MESSAGE_KEY.value, message)
                .build());

        for (Socket socket : connections.keySet()) {
            ReadThread readThread = connections.get(socket);
            if (readThread.isAlive()) {
                readThread.getPrintWriter().println(stringWriter);
            }
        }

        if (message.equals(DISCONNECT.value)) {
            for (Socket socket : connections.keySet()) {
                if (connections.get(socket).isAlive())
                    removeConnection(socket);
            }
            System.out.println(YOU_DISCONNECTED.value);
        }
    }
    public void pauseConnection(Socket socket) throws IOException {
        ReadThread readThread = connections.get(socket);
        if (readThread.isAlive()) {
            readThread.interrupt();
            connections.remove(socket);
            connections.put(socket, new ReadThread(socket));
        }
    }

    public void pauseAllConnections() throws IOException {
        for (Socket socket : connections.keySet()) {
            pauseConnection(socket);
        }
    }

    public void removeConnection(Socket socket) {
        connections.remove(socket);
    }

    public void disconnectFromAll() {
        for (Socket socket : connections.keySet()) {
            awakeConnection(socket.getInetAddress().getHostAddress(), socket.getPort());
        }

        writeMessage(DISCONNECT.value);
        connections.clear();

        System.out.println(DISCONNECTED_FROM_EVERYONE.value);
    }

    public List<Socket> getConnections() {
        return new ArrayList<>(connections.keySet());
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
