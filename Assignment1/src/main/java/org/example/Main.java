package org.example;

import org.example.ui.CmdLineUI;

public class Main {
    public static void main(String[] args) {
        CmdLineUI.getInstance().start();
    }
}